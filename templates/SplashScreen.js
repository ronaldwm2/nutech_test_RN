import { Animated, Button, FlatList, Text, TextInput, View, TouchableNativeFeedback, Image } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import React, { memo, useEffect, useState, useRef } from 'react'
import globalCss from '../GlobalCss/globalCss';
import { client } from '../apiService/client';
import { apiCheckProfile, accountStatusState, tokenStatusState, checkTokenLoadingState, checkTokenCompleteState, accountIsExist } from '../state/userState';
import { SafeAreaView } from 'react-native-safe-area-context';
import { FadeInAnimation } from '../animation';

export const SplashScreen = ({ navigation }) => {
    const [isAnimationDone, setIsAnimationDone] = useState(false);
    const [statusMessage, setStatusMessage] = useState("Loading...");
    const checkTokenCompleted = useSelector(checkTokenCompleteState);
    const checkTokenLoad = useSelector(checkTokenLoadingState);
    const isTokenExpired = useSelector(tokenStatusState);
    const isHaveAccount = useSelector(accountStatusState);
    const dispatch = useDispatch();
    // client.deleteAuthorization();
    // navigation.replace("LandingPage");
    useEffect(() => {
        if (isAnimationDone) {
            if (isHaveAccount) {
                setStatusMessage("Checking Your Token Status...");
                if (!checkTokenCompleted && !checkTokenLoad) {
                    dispatch(apiCheckProfile());
                }
        
                if (checkTokenCompleted && isTokenExpired) {
                    // redirect ke page login
                    client.deleteAuthorization();
                    navigation.replace("LoginPage");
                }
        
                if (checkTokenCompleted && !isTokenExpired) {
                    // redirect ke homepage
                    navigation.replace("LandingPage");
                    
                }
            } else {
                client.getAuthorization().then((authy) => {
                    try {
                        if (authy) {
                            dispatch(accountIsExist())
                        } else {
                            navigation.replace("LoginPage");
                            // redirect ke page login
                        }
                    } catch (e) {
                        navigation.replace("LoginPage");
                        // redirect ke page login
                    }
        
                }, (err) => {
                    // setStatusMessage("User not registered or logged in!");
                    //redirect ke page login
                    navigation.replace("LoginPage");
                })
            }
        }
    }, [isAnimationDone, isHaveAccount, checkTokenCompleted, checkTokenLoad, isTokenExpired, dispatch]);

    return (
        <SafeAreaView>
            <FadeInAnimation
                onAnimationCompleted = {() => {
                    setIsAnimationDone(true);
                }}>
                <View style={globalCss.fullSizeCenterContainer}>
                        <Image
                            source={require('../assets/images/nutech_logo.jpeg')}
                        ></Image>
                        <Text style={{...globalCss.textSecondaryColor, ...globalCss.text18}}>{statusMessage}</Text>
                </View>
            </FadeInAnimation>
        </SafeAreaView>
    )
};