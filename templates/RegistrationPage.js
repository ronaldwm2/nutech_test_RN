import { ToastAndroid, ScrollView, Animated, SafeAreaView, Button, FlatList, Text, TextInput, View, TouchableNativeFeedback, Image } from 'react-native';
import React, {useRef, useEffect, useState} from 'react';
import globalCss from '../GlobalCss/globalCss';
import { FadeInAnimation, FadeOutAnimation, InjectFadeInAnimation, InjectFadeOutAnimation } from '../animation';
import { registrationIsFailed, registrationIsLoading, registrationIsSuccess, apiRegistration, resetRegistrationData } from '../state/userState'
import { useSelector, useDispatch } from 'react-redux'

export const RegistrationPage = ({ navigation }) => { 
    const [email, onChangeEmail] = useState("");
    const [password, onChangePassword] = useState("");
    const [firstName, onChangeFirstName] = useState("");
    const [lastName, onChangeLastName] = useState("");
    const [showPassword, showPasswordToggle] = useState(true);
    const [triggerAnimation, setTriggerAnimation] = useState("none");
    const [isAnimationLaunched, setAnimLaunch] = useState(false);

    const dispatch = useDispatch();
    const isRegistrationSuccess = useSelector(registrationIsSuccess);
    const isRegistrationLoading = useSelector(registrationIsLoading);
    if (isRegistrationSuccess && !isAnimationLaunched) {
        navigation.setOptions({
            headerShown: false
        })
        setTriggerAnimation("LoginPage");
        setAnimLaunch(true);
        dispatch(resetRegistrationData());
    }
    let jsxWithoutAnim = (
        <SafeAreaView>
            <ScrollView contentContainerStyle={{height: "100%"}}>
                <View style={{...globalCss.fullSizeLoginContainer}}>
                    <View
                        style={{...globalCss.fullWidth, ...globalCss.flexCentering}}>
                        <Text
                            style={{...globalCss.bottomMargin8, ...globalCss.textSecondaryColor}}>Email</Text>
                        <TextInput
                            style={globalCss.textBoxLogin}
                            value={email}
                            onChangeText={onChangeEmail}
                            placeholder="Email"
                            placeholderTextColor={globalCss.textPlaceholderColor.color}
                        />
                    </View>
                    <View
                        style={{...globalCss.fullWidth, ...globalCss.flexCentering}}>
                        <Text
                            style={{...globalCss.bottomMargin8, ...globalCss.textSecondaryColor}}>Password</Text>
                        <View
                            style={{position:'relative', width: "75%"}}>
                            <TextInput
                                style={{...globalCss.textBoxLogin, width: "100%"}}
                                value={password}
                                onChangeText={onChangePassword}
                                placeholder="Password"
                                secureTextEntry={showPassword}
                                placeholderTextColor={globalCss.textPlaceholderColor.color}
                            />
                            <Text
                                style={{position:'absolute', right: 15, top: 20, ...globalCss.hyperlinkTexts}}
                                onPress= { () => {
                                    showPasswordToggle(showPassword ? false : true);
                                }}>{showPassword ? "SHOW" : "HIDE"}</Text>
                        </View>
                    </View>

                    <View
                        style={{...globalCss.fullWidth, ...globalCss.flexCentering}}>
                        <Text
                            style={{...globalCss.bottomMargin8, ...globalCss.textSecondaryColor}}>First Name</Text>
                        <TextInput
                            style={globalCss.textBoxLogin}
                            value={firstName}
                            onChangeText={onChangeFirstName}
                            placeholder="First Name"
                            placeholderTextColor={globalCss.textPlaceholderColor.color}
                        />
                    </View>

                    <View
                        style={{...globalCss.fullWidth, ...globalCss.flexCentering}}>
                        <Text
                            style={{...globalCss.bottomMargin8, ...globalCss.textSecondaryColor}}>Last Name</Text>
                        <TextInput
                            style={globalCss.textBoxLogin}
                            value={lastName}
                            onChangeText={onChangeLastName}
                            placeholder="Last Name"
                            placeholderTextColor={globalCss.textPlaceholderColor.color}
                        />
                    </View>

                    <View
                        style={{
                            ...globalCss.fullWidth,
                            ...globalCss.flexCentering,
                            ...globalCss.bottomMargin8,
                            "display" : isRegistrationLoading ? "none" : "flex"}}
                    >
                            <TouchableNativeFeedback
                                onPress={() => {
                                    // setTriggerAnimation("LoginPage")

                                    if (email == "") {
                                        ToastAndroid.show("Email tidak boleh kosong", ToastAndroid.SHORT);
                                        return;
                                    }
                                    if (password == "") {
                                        ToastAndroid.show("Password tidak boleh kosong", ToastAndroid.SHORT);
                                        return;
                                    }
                                    if (firstName == "") {
                                        ToastAndroid.show("First Name tidak boleh kosong", ToastAndroid.SHORT);
                                        return; 
                                    }
                                    if (lastName == "") {
                                        ToastAndroid.show("Last Name tidak boleh kosong", ToastAndroid.SHORT);
                                        return;
                                    }
                                    dispatch(apiRegistration({email: email, password: password, first_name: firstName, last_name: lastName}));
                                }}>
                                <View
                                    style={{...globalCss.buttonContainer}}>
                                    <Text
                                        style={{...globalCss.textWhiteColor}}>Register</Text>
                                </View>
                            </TouchableNativeFeedback>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );

    
    let finalJsx = triggerAnimation != 'none' ? InjectFadeOutAnimation(jsxWithoutAnim, () => {

            navigation.reset({
                index: 0,
                routes: [{ name: triggerAnimation}]
            })
        }) : InjectFadeInAnimation(jsxWithoutAnim);

    
    return (
        finalJsx
    )
};