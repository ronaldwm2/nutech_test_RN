import { TouchableOpacity, TouchableWithoutFeedback, Pressable, Modal, StyleSheet, Button, FlatList, Text, TextInput, View, TouchableNativeFeedback, SafeAreaView, ScrollView, ToastAndroid } from 'react-native';
import globalCss from '../GlobalCss/globalCss';
import React, {useRef, useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { apiCheckProfile, resetUserProfile, isCheckTokenCompletedState, profileLoadingCheckTokenState, fetchLstOfTransactionOnProgressState, fetchTransactionDoneState, fetchLstOfTransactionState, apiTransferBalance, resetUserBalanceData, apiAddBalance, apiCheckBalance, userLastName, userFirstName, userBalanceFetchedInProgressState, userBalance, userBalanceFetchedErrState, userBalanceFetchedState, apiTransactionHistory } from '../state/userState';

// mau buat logic profile nya api nya error terus dan tidak bisa login
// jadinya tidak dibuat

const localCss = StyleSheet.create({
    headerMainpage: {
        width: "100%",
        heigth: 500,
        minHeight: 155,
        maxHeight: 155,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
        marginBottom: 20,
        padding: 20,
        ...globalCss.mainColor
    },
    actionSingles: {
        width: 175,
        height: 100,
        ...globalCss.secondaryColor, 
        ...globalCss.flexCentering, 
        borderRadius: 25, 
        marginRight: 15
    },
    centeredView: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        // marginTop: 22,
        paddingBottom:0,
        margin: 0,
        backgroundColor: "rgba(0,0,0,0.8)"
    },
    modalView: {
        backgroundColor: 'white',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        position: "relative",
        width: "100%",
        height: 500
      }
});

const TransactionHistorySingles = ({date, type, amount}) => {

    return (
        <View
        style={{ width: "100%", flexDirection: "row", padding:15}}>
            <View style={{
                width: "40%"
            }}>
                <Text>{date}</Text>
            </View>
            <View style={{
                width: "25%"
            }}>
                <Text>{type}</Text>
            </View>
            <View style={{
                width: "35%"
            }}>
                <Text>{amount}</Text>
            </View>
        </View>
    )
}
 
export const LandingPage = ({ navigation }) => {
    const [showModalsTopup, setShowModalTopup] = useState(false);
    const [showModalsTransfer, setShowModalsTransfer] = useState(false);
    const [nuTechTopupBalance, setNutechTopupBalance] = useState("");
    const [nuTechTrfBalance, setNutechTrfBalance] = useState("");
    const [isNavigatedBack, setNavigationBack] = useState(false);
    let ufirstName = useSelector(userFirstName);
    let ulastName = useSelector(userLastName);
    let isBalanceFetching = useSelector(userBalanceFetchedInProgressState);
    let balance = useSelector(userBalance);
    let isBalanceFetchingError = useSelector(userBalanceFetchedErrState);
    let isBalanceFetchingDone = useSelector(userBalanceFetchedState);

    let lstOfTransaction = useSelector(fetchLstOfTransactionState);
    let fetchTransactionOnprogress = useSelector(fetchLstOfTransactionOnProgressState)
    let fetchTransactionDone = useSelector(fetchTransactionDoneState);
    
    let isTopupCompleted = useSelector(state => state.userState.balanceTopupCompleted)
    let isTransferCompleted = useSelector(state => state.userState.balanceTransferCompleted);

    let profileLoadingCheckToken = useSelector(profileLoadingCheckTokenState);
    let isCheckTokenCompleted = useSelector(isCheckTokenCompletedState);
    let uFullName = `${ufirstName} ${ulastName}`;
    let dispatch = useDispatch();

    useEffect(() => {
        dispatch(resetUserProfile());
    },[]);

    useEffect(() => {
        if (!isCheckTokenCompleted) {
            if (!profileLoadingCheckToken) {
                dispatch(apiCheckProfile());
            }
        }
    }, [profileLoadingCheckToken, isCheckTokenCompleted, dispatch])

    useEffect(() => {
        console.log(isNavigatedBack);
        if (!isBalanceFetchingDone) {
            if (!isBalanceFetching) {
                dispatch(apiCheckBalance());
            }
        }
    }, [isBalanceFetching, isBalanceFetchingDone, isBalanceFetchingError]);

    if (isBalanceFetchingError && !isNavigatedBack) {
        //     // kl error
            dispatch(resetUserProfile());
            ToastAndroid.show("Token is invalid, redirecting", ToastAndroid.SHORT);
            setNavigationBack(true);
            navigation.reset({
                index: 0,
                routes: [{ name: "SplashScreen"}]
            })
            // navigation.replace("SplashScreen");
        }

    useEffect(() => {
        if (isTopupCompleted || isTransferCompleted) {
            dispatch(resetUserBalanceData());
        }
    }, [isTopupCompleted, isTransferCompleted, dispatch]);


    useEffect(() => {
        if (!fetchTransactionDone) {
            if (!fetchTransactionOnprogress) {
                dispatch(apiTransactionHistory());
            }
        }
    }, [fetchTransactionDone, fetchTransactionOnprogress, dispatch]);

    let lstOfTransactionJSX = [];
    for (const row of lstOfTransaction) {
        lstOfTransactionJSX.push(
            <TransactionHistorySingles
                type={row.transaction_type}
                amount={row.amount}
                date={row.transaction_time}
                key={row.transaction_id}>

            </TransactionHistorySingles>
        )
    }

    return (
    <SafeAreaView>
        <View style={{display: "flex"}}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={showModalsTopup}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <TouchableOpacity
                    style={localCss.centeredView}
                    activeOpacity={1}
                    onPress={() => {
                        setShowModalTopup(false);
                    }}    
                >
                    <TouchableOpacity
                        activeOpacity={1}
                        style={[localCss.modalView]}> 
                            <Pressable
                                onPress={() => {
                                    setShowModalTopup(false);
                                }}
                                style={{position: "absolute", right: 25, top: 15}}>
                                <Text style={[globalCss.textBold, globalCss.text18]}>X</Text>
                            </Pressable>
                            <Text
                                style={[globalCss.text26, globalCss.textBlack, globalCss.textBold, globalCss.bottomMargin15]}>Top Up NuTech Balance</Text>
                            <Pressable>
                                <Text>Balance to top up</Text>
                            </Pressable>
                            <TextInput
                                    style={globalCss.textBoxLogin}
                                    value={nuTechTopupBalance}
                                    onChangeText={(text) => {
                                        let sanitizedText = text.replace(/[^0-9]/g, "");
                                        setNutechTopupBalance(sanitizedText);
                                        
                                    }}
                                    keyboardType='numeric'
                                    placeholder="Balance to topup"
                                    placeholderTextColor={globalCss.textPlaceholderColor.color}
                            />
                            <TouchableNativeFeedback
                                onPress={() => {
                                    if (Number(nuTechTopupBalance) < 10000) {
                                        ToastAndroid.show("Minimal Topup 10000", ToastAndroid.SHORT);
                                        return;
                                    }
                                    dispatch(apiAddBalance({amount: nuTechTopupBalance}));
                                    // dispatch(resetUserBalanceData());
                                    // setShowModalTopup(false);
                                }}>
                                <View
                                    style={{...globalCss.buttonContainer}}>
                                    <Text
                                        style={{...globalCss.textWhiteColor}}>Top Up</Text>
                                </View>
                            </TouchableNativeFeedback>
                    </TouchableOpacity>
                </TouchableOpacity>

            </Modal>
            <Modal
                animationType="fade"
                transparent={true}
                visible={showModalsTransfer}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <TouchableOpacity
                    style={localCss.centeredView}
                    activeOpacity={1}
                    onPress={() => {
                        setShowModalsTransfer(false);
                    }}    
                >
                    <TouchableOpacity
                        activeOpacity={1}
                        style={[localCss.modalView]}> 
                            <Pressable
                                onPress={() => {
                                    setShowModalsTransfer(false);
                                }}
                                style={{position: "absolute", right: 25, top: 15}}>
                                <Text style={[globalCss.textBold, globalCss.text18]}>X</Text>
                            </Pressable>
                            <Text
                                style={[globalCss.text26, globalCss.textBlack, globalCss.textBold, globalCss.bottomMargin15]}>Transfer NuTech Balance</Text>
                            <Pressable>
                                <Text>Balance to transfer</Text>
                            </Pressable>
                            <TextInput
                                    style={globalCss.textBoxLogin}
                                    value={nuTechTrfBalance}
                                    onChangeText={(text) => {
                                        let sanitizedText = text.replace(/[^0-9]/g, "");
                                        setNutechTrfBalance(sanitizedText);
                                        
                                    }}
                                    keyboardType='numeric'
                                    placeholder="Balance to transfer"
                                    placeholderTextColor={globalCss.textPlaceholderColor.color}
                            />
                            <TouchableNativeFeedback
                                onPress={() => {
                                    if (Number(nuTechTrfBalance) < 10000) {
                                        ToastAndroid.show("Minimal Transfer 10000", ToastAndroid.SHORT);
                                        return;
                                    }

                                    dispatch(apiTransferBalance({amount: nuTechTrfBalance}));
                                    // dispatch(resetUserBalanceData());
                                    // setShowModalsTransfer(false);
                                }}>
                                <View
                                    style={{...globalCss.buttonContainer}}>
                                    <Text
                                        style={{...globalCss.textWhiteColor}}>Transfer</Text>
                                </View>
                            </TouchableNativeFeedback>
                    </TouchableOpacity>
                </TouchableOpacity>

            </Modal>
            <View
                style={localCss.headerMainpage}>
                    <View
                        style={{flexDirection: 'row', position: 'relative'}}>
                        <Text
                            style={{...globalCss.textWhiteColor, ...globalCss.text18, ...globalCss.textBold, ...globalCss.bottomMargin15 }}>Hello {uFullName}</Text>
                        <Text
                            style={{...globalCss.hyperlinkTexts, position: "absolute", right: 0, top: 3}}>Change Profile</Text>
                    </View>

                    <Text
                        style={{...globalCss.textWhiteColor, ...globalCss.text18, ...globalCss.textBold }}>My NuTech Balance</Text>
                    <Text
                        style={{...globalCss.textWhiteColor, ...globalCss.text26, ...globalCss.textBold }}>Rp. {balance}</Text>
            </View>
            <View
                style={{padding: 10, width: "100%", minHeight: 125, maxHeight: 125, justifyContent:"center", flexDirection:"row"}}>
                <TouchableNativeFeedback
                    onPress={() => {
                        setShowModalTopup(true);
                    }}>
                    <View
                        style={localCss.actionSingles}>
                        <Text
                            style={{...globalCss.textWhiteColor}}>Top Up</Text>
                    </View>
                </TouchableNativeFeedback>
                <TouchableNativeFeedback
                        onPress={() => {
                            setShowModalsTransfer(true);
                        }}>
                        <View
                            style={localCss.actionSingles}>
                            <Text
                                style={{...globalCss.textWhiteColor}}>Transfer</Text>
                        </View>
                </TouchableNativeFeedback>
            </View>
            <Text
                style={{textAlign: "center", fontSize: 24, ...globalCss.textBlack, ...globalCss.bottomMargin15}}>Recent Transaction</Text>
            <View
                style={{ width: "100%", flexDirection: "row", padding:15, borderBottomWidth: 2}}>
                    <View style={{
                        width: "40%",
                    }}>
                        <Text>Date</Text>
                    </View>
                    <View style={{
                        width: "25%"
                    }}>
                        <Text>Transfer / Topup</Text>
                    </View>
                    <View style={{
                        width: "35%"
                    }}>
                        <Text>Amount</Text>
                    </View>
            </View>
            <View
                style={{height: 400}}>
                <ScrollView>
                    <View
                        style={{flex:1}}>
                            {lstOfTransactionJSX}
                    </View>
                </ScrollView>
            </View>
        </View>

    </SafeAreaView>)
};