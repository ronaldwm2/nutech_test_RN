import { ScrollView, Animated, SafeAreaView, Button, FlatList, Text, TextInput, View, TouchableNativeFeedback, Image, ToastAndroid } from 'react-native';
import React, {useRef, useEffect, useState} from 'react';
import globalCss from '../GlobalCss/globalCss';
import { FadeInAnimation, FadeOutAnimation, InjectFadeInAnimation, InjectFadeOutAnimation } from '../animation';
import { useSelector, useDispatch } from 'react-redux'
import { apiLogin, loginIsSuccess, loginJwtToken, loginLoadingState } from '../state/userState';
import { client } from '../apiService/client';

export const LoginPage = ({ navigation }) => { 
    const [email, onChangeEmail] = useState("");
    const [password, onChangePassword] = useState("");
    const [showPassword, showPasswordToggle] = useState(true);
    const [triggerAnimation, setTriggerAnimation] = useState("none");
    const [triggerLogin, setTriggerLogin] = useState(false);
    const dispatch = useDispatch();
    const isLoginSuccess = useSelector(loginIsSuccess);
    const isLoginAPICompleted = useSelector(loginLoadingState);
    const loginToken = useSelector(loginJwtToken);
    // navigation.replace("SplashScreen")
    if (isLoginSuccess && !triggerLogin) {
        setTriggerLogin(true);
        client.setAuthorization(loginToken).then(() => {
            navigation.replace("LandingPage")
        })
    }

    let jsxWithoutAnim = (
    <SafeAreaView>
        <ScrollView contentContainerStyle={{height: "100%"}}>
            <View style={{...globalCss.fullSizeLoginContainer}}>
                <Image
                    source={require('../assets/images/nutech_logo.jpeg')}
                ></Image>
                <View
                    style={{...globalCss.fullWidth, ...globalCss.flexCentering}}>
                    <Text
                        style={{...globalCss.bottomMargin8, ...globalCss.textSecondaryColor}}>Email</Text>
                    <TextInput
                        style={globalCss.textBoxLogin}
                        value={email}
                        onChangeText={onChangeEmail}
                        placeholder="Email"
                        placeholderTextColor={globalCss.textPlaceholderColor.color}
                    />
                </View>
                <View
                    style={{...globalCss.fullWidth, ...globalCss.flexCentering}}>
                    <Text
                         style={{...globalCss.bottomMargin8, ...globalCss.textSecondaryColor}}>Password</Text>
                    <View
                        style={{position:'relative', width: "75%"}}>
                        <TextInput
                            style={{...globalCss.textBoxLogin, width: "100%"}}
                            value={password}
                            onChangeText={onChangePassword}
                            placeholder="Password"
                            secureTextEntry={showPassword}
                            placeholderTextColor={globalCss.textPlaceholderColor.color}
                        />
                        <Text
                            style={{position:'absolute', right: 15, top: 20, ...globalCss.hyperlinkTexts}}
                            onPress= { () => {
                                showPasswordToggle(showPassword ? false : true);
                            }}>{showPassword ? "SHOW" : "HIDE"}</Text>
                    </View>
                </View>

                <View
                    style={{...globalCss.fullWidth, ...globalCss.flexCentering, ...globalCss.bottomMargin8}}>
                        <TouchableNativeFeedback
                            onPress={() => {
                                if (email == "") {
                                    ToastAndroid.show("Email tidak boleh kosong", ToastAndroid.SHORT);
                                    return;
                                }
                                if (password == "") {
                                    ToastAndroid.show("Password tidak boleh kosong", ToastAndroid.SHORT);
                                    return;
                                }
                                dispatch(apiLogin({email:email, password:password}))
                            }}>
                            <View
                                style={{...globalCss.buttonContainer, display: isLoginAPICompleted ? "none" : "flex"}}>
                                <Text
                                    style={{...globalCss.textWhiteColor}}>LOGIN</Text>
                            </View>
                        </TouchableNativeFeedback>
                </View>
                <Text
                    style={globalCss.textSecondaryColor}>Don't have account yet? </Text>
                <Text
                    style={globalCss.hyperlinkTexts}
                    onPress= { () => {
                        setTriggerAnimation('RegistrationPage');
                    }}>Register here</Text>
            </View>
        </ScrollView>
    </SafeAreaView>
    );
    
    let finalJsx = triggerAnimation != 'none' ? InjectFadeOutAnimation(jsxWithoutAnim,() => {
        navigation.navigate(triggerAnimation);
        setTimeout(() => {
            setTriggerAnimation('none');
        }, 200)   
    }) : InjectFadeInAnimation(jsxWithoutAnim);

    
    return (
        finalJsx
    )
};