import { configureStore, combineReducers } from '@reduxjs/toolkit'
import registrationPageSlice from './state/userState';
import { globalUserStateReducer, registrationPageReducer, loginStateReducer } from './state/userState';

const combinedReducer = combineReducers({
    registrationPageSlice: registrationPageReducer,
    userState: globalUserStateReducer,
    loginState: loginStateReducer
})

const rootReducer = (state, action) => {
  return combinedReducer(state, action);
}

export default configureStore({
  reducer: rootReducer
})