import AsyncStorage from '@react-native-async-storage/async-storage';

export async function client(endpoint, { body, ...customConfig } = {}) {
    const headers = { 'Content-Type': 'application/json' }
  
    const config = {
      method: body ? 'POST' : 'GET',
      ...customConfig,
      headers: {
        ...headers,
        ...customConfig.headers,
      },
    }
    if (body) {
      config.body = JSON.stringify(body)
    }
  
    let data
    try {
      const response = await window.fetch(endpoint, config)
      data = await response.json()
      if (response.ok) {
        // Return a result object similar to Axios
        return {
          status: response.status,
          data,
          headers: response.headers,
          url: response.url,
        }
      }
      throw new Error(response.statusText)
    } catch (err) {
      return Promise.reject(err.message ? err.message : data)
    }
  }

  client.deleteAuthorization = async function () {
    return new Promise(async (resolve, reject) => {
        try {
            await AsyncStorage.removeItem("jwtTokenAuth");
            resolve();
        } catch (error) {
            reject(error);
        }
        
    })
  }

  client.getAuthorization = async function () {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await AsyncStorage.getItem("jwtTokenAuth");
            if (data) {
                global.jwtToken = data;
                resolve(data);
            } else {
                reject({message: "Authorization not found or expired!"});
            }
        } catch (error) {
            reject(error);
        }
        
    })
  }

  client.setAuthorization = async function (jwtToken) {
    return new Promise(async (resolve, reject) => {
        try {
            await AsyncStorage.setItem("jwtTokenAuth", jwtToken);
            global.jwtToken = jwtToken;
            resolve({
              success: true
            })
        } catch (error) {
            reject({
              success: false,
              reason: error.message
            });
        }
        
    })
  }

  client.baseUrl = `https://tht-api.nutech-integrasi.app/`;
  
  client.get = function (endpoint, customConfig = {}) {
    return client(endpoint, { ...customConfig, method: 'GET' })
  }
  
  client.post = function (endpoint, body, customConfig = {}) {
    return client(endpoint, { ...customConfig, body })
  }
  