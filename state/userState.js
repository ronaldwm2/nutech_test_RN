import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { client } from '../apiService/client'
import { ToastAndroid, Modal } from 'react-native'
import { useSelector} from 'react-redux'

const status = [102, 103, 108];

// API DEC
export const apiRegistration = createAsyncThunk(client.baseUrl + `registration`, async ({email, password, first_name, last_name}) => {
    let objectToPost = {
        email: email,
        password: password,
        first_name: first_name,
        last_name: last_name
    }
    const response = await client.post(client.baseUrl + `registration`, objectToPost);
    return response.data;
})

export const apiLogin = createAsyncThunk(client.baseUrl + `login`, async ({email, password}) => {
    let objectToPost = {
        email: email,
        password: password
    }
    const response = await client.post(client.baseUrl + `login`, objectToPost);
    return response.data;
})

export const apiUpdateProfile = createAsyncThunk(client.baseUrl + `updateProfile`, async (first_name, last_name) => {
    const jwtToken = global.jwtToken;
    const auth = jwtToken ? jwtToken : await client.getAuthorization();
    let objectToPost = {
        first_name: first_name,
        last_name: last_name
    }
    const response = await client.post(client.baseUrl + `login`, objectToPost, {
        headers: {
            Authorization: "Bearer " + auth
        }
    });
    return response.data;
})

export const apiCheckProfile = createAsyncThunk(client.baseUrl + `getProfile`, async () => {
    const jwtToken = global.jwtToken;
    const auth = jwtToken ? jwtToken : await client.getAuthorization();
    
    const response = await client.get(client.baseUrl + `getProfile`, {
        headers: {
            Authorization: "Bearer " + auth
        }
    });
    return response.data;
})

export const apiCheckBalance = createAsyncThunk(client.baseUrl + `balance`, async () => {
    const jwtToken = global.jwtToken;
    const auth = jwtToken ? jwtToken : await client.getAuthorization();
    
    const response = await client.get(client.baseUrl + `balance`, {
        headers: {
            Authorization: "Bearer " + auth
        }
    });
    return response.data;
})

export const apiAddBalance = createAsyncThunk(client.baseUrl + `topup`, async ({amount}) => {
    const jwtToken = global.jwtToken;
    const auth = jwtToken ? jwtToken : await client.getAuthorization();
    const payload = {
        amount: amount
    }
    const response = await client.post(client.baseUrl + `topup`, payload, {
        headers: {
            Authorization: "Bearer " + auth
        }
    });
    return response.data;
})

export const apiTransferBalance = createAsyncThunk(client.baseUrl + `transfer`, async ({amount}) => {
    const jwtToken = global.jwtToken;
    const auth = jwtToken ? jwtToken : await client.getAuthorization();
    const payload = {
        amount: amount
    }
    const response = await client.post(client.baseUrl + `transfer`, payload, {
        headers: {
            Authorization: "Bearer " + auth
        }
    });
    return response.data;
})

export const apiTransactionHistory = createAsyncThunk(client.baseUrl + `transactionHistory`, async () => {
    const jwtToken = global.jwtToken;
    const auth = jwtToken ? jwtToken : await client.getAuthorization();

    const response = await client.get(client.baseUrl + `transactionHistory`, {
        headers: {
            Authorization: "Bearer " + auth
        }
    });
    return response.data;
})

// API DEC DONE

// STATE DEC
const loginStateSlice = createSlice({
    name: 'loginState',
    initialState: {
        isSuccess: false,
        jwtToken: "",
        loadingCheckLogin: false
    },
    reducers: {

    },
    extraReducers(builder) {
        builder.addCase(apiLogin.pending, (state, action) => {
            state.isSuccess = false;
            state.loadingCheckLogin = true;
        })
        .addCase(apiLogin.fulfilled, (state, action) => {
            state.loadingCheckLogin = false;
            state.isSuccess = true;
            state.jwtToken = action.payload.data.token;
        })
        .addCase(apiLogin.rejected, (state, action) => {
            ToastAndroid.show(action.error.message, ToastAndroid.SHORT);
            state.loadingCheckLogin = false;
            state.isSuccess = false;
        })
    }
})

const globalUserState = createSlice({
    name: 'userState',
    initialState: {
        tokenExpired: false,
        hasAccountSaved: false,
        loadingCheckToken: false,
        isCheckTokenCompleted: false,
        userJwtToken: "",
        firstName: "",
        lastName: "",
        email: "",
        balance: 0,
        balanceFetched: false,
        balanceFetchErr: false,
        balanceFetchInProgress: false,
        balanceTopupInProgress: false,
        balanceTopupErr: false,
        balanceTopupCompleted: false,
        balanceTransferInProgress: false,
        balanceTransferErr: false,
        balanceTransferCompleted: false,
        lstOfTransactions: [],
        fetchTransactionDone: false,
        fetchTransactionErr: false,
        fetchTransactionInProgress: false
    },
    reducers: {
        tokenIsExpired: (state) => {
            state.tokenExpired = true;
        },
        accountIsExist: (state) => {
            state.hasAccountSaved = true;
        },
        resetUserState: (state) => {
            return {
                tokenExpired: false,
                hasAccountSaved: false,
                loadingCheckToken: false,
                isCheckTokenCompleted: false,
                userJwtToken: "",
                firstName: "",
                lastName: "",
                email: "",
                balance: 0,
                balanceFetched: false,
                balanceFetchErr: false,
                balanceFetchInProgress: false,
                balanceTopupInProgress: false,
                balanceTopupErr: false,
                balanceTopupCompleted: false,
                balanceTransferInProgress: false,
                balanceTransferErr: false,
                balanceTransferCompleted: false
            }
        },
        setUserJwtToken: (state) => {
            state.userJwtToken = userJwtToken;
        },
        resetUserBalanceData: (state) => {
            state.balance = 0;
            state.balanceFetched = false;
            state.balanceFetchErr = false;
            state.balanceFetchInProgress = false;

            state.balanceTopupInProgress= false,
            state.balanceTopupErr= false,
            state.balanceTopupCompleted= false,
            state.balanceTransferInProgress= false,
            state.balanceTransferErr= false,
            state.balanceTransferCompleted= false

            state.lstOfTransactions = [];
            state.fetchTransactionDone = false;
            state.fetchTransactionErr = false;
            state.fetchTransactionInProgress = false;
        },
        resetUserProfile: (state) => {
            state.loadingCheckToken = false;
            state.isCheckTokenCompleted = false;
            state.firstName = "";
            state.lastName = "";
            state.email = "";
        }
    },
    extraReducers(builder) {
        builder.addCase(apiCheckProfile.pending, (state, action) => {
            state.loadingCheckToken = true;
            state.isCheckTokenCompleted = false;
        })
        .addCase(apiCheckProfile.fulfilled, (state, action) => {
            state.tokenExpired = false;
            state.loadingCheckToken = false;
            state.isCheckTokenCompleted = true;
            state.firstName = action.payload.data.first_name;
            state.lastName = action.payload.data.last_name;
            state.email = action.payload.data.email;
        })
        .addCase(apiCheckProfile.rejected, (state, action) => {
            state.tokenExpired = true;
            state.loadingCheckToken = false;
            state.isCheckTokenCompleted = true;
        })
        .addCase(apiCheckBalance.pending, (state, action) => {
            state.balanceFetched = false;
            state.balanceFetchInProgress = true;
            state.balanceFetchErr = false;
        })
        .addCase(apiCheckBalance.fulfilled, (state, action) => {
            state.balanceFetchInProgress = false;
            state.balanceFetchErr = false;
            let findData = status.find((e) => e == action.payload.status);
            if (findData) {
                state.balanceFetched = false;
            } else {
                state.balanceFetched = true;
            }
            state.balance = action.payload.data.balance ? action.payload.data.balance : 0;
        })
        .addCase(apiCheckBalance.rejected, (state, action) => {
            state.balanceFetched = true;
            state.balanceFetchInProgress = false;
            state.balanceFetchErr = true;
        })
        .addCase(apiAddBalance.pending, (state, action) => {
            state.balanceTopupInProgress = true;
            state.balanceTopupCompleted = false;
            state.balanceTopupErr = false;
        })
        .addCase(apiAddBalance.fulfilled, (state, action) => {
            state.balanceTopupInProgress = false;
            state.balanceTopupCompleted = true;
            state.balanceTopupErr = false;
        })
        .addCase(apiAddBalance.rejected, (state, action) => {
            state.balanceTopupInProgress = false;
            state.balanceTopupCompleted = false;
            state.balanceTopupErr = true;
        })
        .addCase(apiTransferBalance.pending, (state, action) => {
            state.balanceTransferInProgress = true;
            state.balanceTransferCompleted = false;
            state.balanceTransferErr = false;
        })
        .addCase(apiTransferBalance.fulfilled, (state, action) => {
            state.balanceTransferInProgress = false;
            state.balanceTransferCompleted = true;
            state.balanceTransferErr = false;
        })
        .addCase(apiTransferBalance.rejected, (state, action) => {
            state.balanceTransferInProgress = false;
            state.balanceTransferCompleted = false;
            state.balanceTransferErr = true;
        })
        .addCase(apiTransactionHistory.pending, (state, action) => {
            state.fetchTransactionInProgress = true;
            state.fetchTransactionErr = false;
            state.fetchTransactionDone = false;
        })
        .addCase(apiTransactionHistory.fulfilled, (state, action) => {
            state.fetchTransactionInProgress = false;
            state.fetchTransactionErr = false;
            state.fetchTransactionDone = true;
            state.lstOfTransactions = action.payload.data;
        })
        .addCase(apiTransactionHistory.rejected, (state, action) => {
            state.fetchTransactionInProgress = false;
            state.fetchTransactionErr = true;
            state.fetchTransactionDone = true;
        })
    }
})

const registrationPageSlice = createSlice({
    name: 'registrationPageSlice',
    initialState : {
        isSuccess: false,
        isLoading: false,
        isFailed: false
    },
    reducers: {
        resetRegistrationData: (state) => {
            // state = {
            //     isSuccess: false,
            //     isLoading: false,
            //     isFailed: false
            // }
            state.isFailed = false;
            state.isSuccess = false;
            state.isFailed = false;
        }
    }, 
    extraReducers(builder) {
        builder.addCase(apiRegistration.pending, (state, action) => {
            state.isSuccess = false;
            state.isLoading = true;
            state.isFailed = false;
        })
        .addCase(apiRegistration.fulfilled, (state, action) => {
            // state.isSuccess = true;
            state.isLoading = false;
            state.isFailed = false;
            let findData = status.find((e) => e == action.payload.status);
            if (findData) {
                state.isSuccess = false;
                ToastAndroid.show(action.payload.message, ToastAndroid.SHORT);
            } else {
                state.isSuccess = true;
                ToastAndroid.show(action.payload.message, ToastAndroid.SHORT);
            }
        })
        .addCase(apiRegistration.rejected, (state, action) => {
            state.isLoading = false;
            state.isFailed = true;
            state.isSuccess = false;
            ToastAndroid.show(action.error.message, ToastAndroid.SHORT);
        })
    }
})
// STATE DEC DONE


export const { resetRegistrationData } = registrationPageSlice.actions;
export const { tokenIsExpired, accountIsExist, resetUserState, resetUserBalanceData, resetUserProfile } = globalUserState.actions;
export const {  } = loginStateSlice.actions;

export const registrationPageReducer = registrationPageSlice.reducer;
export const globalUserStateReducer = globalUserState.reducer;
export const loginStateReducer = loginStateSlice.reducer;

export const jwtTokenUser = state => state.userState.userJwtToken;

export const loginIsSuccess = state => state.loginState.isSuccess;
export const loginJwtToken = state => state.loginState.jwtToken;
export const loginLoadingState = state => state.loginState.loadingCheckLogin;

export const registrationIsSuccess = state => state.registrationPageSlice.isSuccess;
export const registrationIsLoading = state => state.registrationPageSlice.isLoading;
export const registrationIsFailed = state => state.registrationPageSlice.isFailed;

export const tokenStatusState = state => state.userState.tokenExpired;
export const accountStatusState = state => state.userState.hasAccountSaved;
export const checkTokenLoadingState = state => state.userState.loadingCheckToken;
export const checkTokenCompleteState = state => state.userState.isCheckTokenCompleted;
export const userFirstName = state => state.userState.firstName;
export const userLastName = state => state.userState.lastName;
export const userEmail = state => state.userState.email;
export const userBalance = state => state.userState.balance;
export const userBalanceFetchedState = state => state.userState.balanceFetched;
export const userBalanceFetchedErrState = state => state.userState.balanceFetchErr;
export const userBalanceFetchedInProgressState = state => state.userState.balanceFetchInProgress;
export const fetchTransactionDoneState = state => state.userState.fetchTransactionDone;
export const fetchLstOfTransactionState = state => state.userState.lstOfTransactions;
export const fetchLstOfTransactionOnProgressState = state => state.userState.fetchTransactionInProgress;

export const profileLoadingCheckTokenState = state => state.userState.loadingCheckToken;
export const isCheckTokenCompletedState = state => state.userState.isCheckTokenCompleted;