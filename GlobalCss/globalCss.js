import { StyleSheet } from "react-native";

export default globalCss = StyleSheet.create({
    mainColor: {
        backgroundColor: "#f47b20",
        color: 'white'
    },
    secondaryColor: {
        backgroundColor: "#4c4d4f"
    },
    fullSizeCenterContainer: {
        display: "flex",
        width: "100%",
        height: "100%",
        alignItems: "center",
        alignContent: "center",
        justifyContent: "center",
        backgroundColor: "#FFF"
    },
    fullSizeLoginContainer: {
        width: "100%",
        height: "100%",
        alignItems: "center",
        alignContent: "center",
        paddingTop: 50,
        backgroundColor: "#FFF"
    },
    text18: {
        fontSize: 18
    },
    text26: {
        fontSize: 26
    },
    textBlack: {
        color: "black"
    },
    textMainColor: {
        backgroundColor: "#f47b20"
    },
    textSecondaryColor: {
        color: "#4c4d4f"
    },
    textPlaceholderColor: {
        color: "#808080"
    },
    textBoxLogin: {
        padding: 15,
        width: "75%",
        borderWidth: 1,
        borderRadius: 10,
        marginBottom: 15,
        borderColor: "#4c4d4f"
    },
    fullWidth: {
        width: "100%"
    },
    flexCentering: {
        alignItems: "center",
        alignContent: "center",
        justifyContent: "center",
        display: "flex",
    },
    bottomMargin8: {
        marginBottom: 8
    },
    bottomMargin15: {
        marginBottom: 15
    },
    buttonContainer: {
        width: "75%",
        height: 50,
        backgroundColor: "#f47b20",
        borderRadius: 10,
        alignItems: "center",
        alignContent: "center",
        justifyContent: "center",
        display: "flex"
    },
    textWhiteColor: {
        color: "#FAFAFA"
    },
    hyperlinkTexts: {
        color: "blue",
        textDecorationLine: 'underline'
    },
    textBold: {
        fontWeight: '800'
    }
});