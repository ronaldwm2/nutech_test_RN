/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import store_config from './store';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { LandingPage } from './templates/LandingPage';
import { SplashScreen } from './templates/SplashScreen';
import globalCss from './GlobalCss/globalCss';
import { LoginPage } from './templates/LoginPage';
import { RegistrationPage } from './templates/RegistrationPage';

const Stack = createNativeStackNavigator();

function App(): JSX.Element {
  return (
    <Provider store={store_config}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="SplashScreen">
          <Stack.Screen
            name="SplashScreen"
            component={SplashScreen}
            options= {{ 
              headerShown: false
            }}
          />
          <Stack.Screen
            name="LoginPage"
            component={LoginPage}
            options={{ 
              headerStyle: globalCss.mainColor,
              headerShown: false
            }}
          ></Stack.Screen>
          <Stack.Screen
            name="RegistrationPage"
            component={RegistrationPage}
            options={{ 
              headerStyle: globalCss.mainColor,
              headerTintColor: "#fff",
              title: "Register new user"
            }}
          ></Stack.Screen>
          <Stack.Screen
            name="LandingPage"
            component={LandingPage}
            options={{
              headerShown: false
            }}
          ></Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;
