import { Animated, Button, FlatList, Text, TextInput, View, TouchableNativeFeedback, Image } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import React, { memo, useEffect, useState, useRef } from 'react'

export const FadeInAnimation = (props) => {
    const fadeAnim = useRef(new Animated.Value(0)).current;
    useEffect(() => {
        Animated.timing(fadeAnim, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
        }).start(props.onAnimationCompleted);
      }, [fadeAnim]);

    return (
        <Animated.View
            style={{
                opacity: fadeAnim
            }}
        >{props.children}</Animated.View>
    )
}

export const InjectFadeInAnimation = (jsx, callbacks = () => {}) => {
    return <FadeInAnimation
        onAnimationCompleted={callbacks}>
        {jsx}
    </FadeInAnimation>
};

export const InjectFadeOutAnimation = (jsx, callbacks = () => {}) => {
    return <FadeOutAnimation
        onAnimationCompleted={callbacks}>
        {jsx}
    </FadeOutAnimation>
};

export const FadeOutAnimation = (props) => {
    const fadeAnim = useRef(new Animated.Value(1)).current;
    useEffect(() => {
        Animated.timing(fadeAnim, {
          toValue: 0,
          duration: 1000,
          useNativeDriver: true,
        }).start(props.onAnimationCompleted);
      }, [fadeAnim]);

    return (
        <Animated.View
            style={{
                opacity: fadeAnim
            }}
        >{props.children}</Animated.View>
    )
}
